﻿using UnityEngine;
using UnityEngine.UI;

public class TipsManager : MonoBehaviour
{
#if NGUI
	public UILabel label;
#else
	public Text label;
#endif
    private static int lastTipNum = 0;
	private static int tipsCount = -1;

	void Awake ()
	{
		if (tipsCount != -1)
			return;

		for (tipsCount = 0;; ++tipsCount) {
			string key = "Tip" + tipsCount;
			if (!Localization.Exists (key))
				break;
		}

	}

    public void Start() {
        
        ShowNextTip();
    }

	public void ShowNextTip ()
	{
	    if (label == null) {
#if NGUI
            label = GetComponent<UILabel>();
#else
	        label = GetComponent<Text>();
#endif
	    }
        lastTipNum = PlayerPrefs.GetInt("lastTipNum", 0);
		label.text = Localization.Get ("Tip" + lastTipNum);
        
        if (tipsCount > 0)
	        lastTipNum = (lastTipNum + 1)%tipsCount;
        PlayerPrefs.SetInt("lastTipNum", lastTipNum);
	}
}
